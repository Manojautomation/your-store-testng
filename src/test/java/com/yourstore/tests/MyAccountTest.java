package com.yourstore.tests;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.yourstore.resources.YourStoreBase;

public class MyAccountTest extends YourStoreBase{
	
Logger log = LogManager.getLogger(LoginTest.class);
	
	@BeforeMethod
	public void openApplication() throws IOException{
		
		intializeDriver();
		log.debug("Browser got launched");
		driver.get(prop.getProperty("url"));
		log.debug("Navigated to application URL");
	}
	
	@Test
	public void myAccount(){
		
		System.out.println("myAccount Page");
		
	}
 
	@AfterMethod
	public void tearDown() {
		driver.close();

	}

}
