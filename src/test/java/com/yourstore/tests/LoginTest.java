package com.yourstore.tests;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.yourstore.pagefactory.AccountPage;
import com.yourstore.pagefactory.LandingPage;
import com.yourstore.pagefactory.LoginPage;
import com.yourstore.resources.YourStoreBase;

public class LoginTest extends YourStoreBase{
	
	Logger log = LogManager.getLogger(LoginTest.class);
	
		
	
	@BeforeMethod
	public void openApplication() throws IOException{
		
		intializeDriver();
		log.debug("Browser got launched");
		driver.get(prop.getProperty("url"));
		log.debug("Navigated to application URL");
	}
	
	@Test(dataProvider="getLoginData")
	public void login(String email,String password,String expectedResult) throws IOException, InterruptedException {

		log = LogManager.getLogger(LoginTest.class.getName());
				
		LandingPage landingpage = new LandingPage(driver);
		landingpage.myAccountDropdown().click();
		log.debug("Clicked on My Account dropdown");
		landingpage.loginOption().click();
		log.debug("Clicked on login option");
		
		LoginPage loginpage = new LoginPage(driver);
		loginpage.emailAddress().sendKeys(email);
		log.debug("Email addressed got entered");
		loginpage.password().sendKeys(password);
		log.debug("Password got entered");
		loginpage.loginBtn().click();
		log.debug("Clicked on Login Button");
		
		AccountPage accountpage = new AccountPage(driver);
		
		String actualResult = null;
		try {
			
			if(accountpage.editAccountInformation().isDisplayed()) {
				log.debug("User got logged in");
				actualResult="Successfull";
			}
		
		} catch (Exception e) {
			log.debug("User didn't log in");
			actualResult="Failure";
		}
		
		if(expectedResult.equals(actualResult)) {
			log.debug("Login Test got passed");
			Assert.assertTrue(true);
		}else {
			log.debug("Login Test Failed");
			Assert.assertTrue(false);
		}

	}
	
	@DataProvider
	public Object[][] getLoginData(){
		
		Object[][] data = {{"manojnvautomation@gmail.com","manoj2017","Successfull"},{"test@gamil.com","123456","Failure"}};
		
		return data;
		
	}
	
	@AfterMethod
	public void tearDown() {
		driver.close();
		
	}
}
