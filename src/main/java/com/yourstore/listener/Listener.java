package com.yourstore.listener;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.yourstore.resources.YourStoreBase;
import com.yourstore.utilities.ExtentReport;

public class Listener extends YourStoreBase implements ITestListener {
	
	ExtentReports extentReport = ExtentReport.getExtentReport();
	ExtentTest extentTest;
	ThreadLocal<ExtentTest> extentTestThread = new ThreadLocal<ExtentTest>();

	@Override
	public void onTestStart(ITestResult result) {
		
		extentTest = extentReport.createTest(result.getName()+" execution started");
		extentTestThread.set(extentTest);
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		
	//	extentTest.log(Status.PASS,result.getName()+"Test Passed");
		extentTestThread.get().log(Status.PASS,result.getName()+"Test Passed");
	}

	@Override
	public void onTestFailure(ITestResult result) {

		
		/*
		 * String testMethodName = result.getName();
		 * 
		 * try {
		 * 
		 * driver = (WebDriver)
		 * result.getTestClass().getRealClass().getDeclaredField("driver")
		 * .get(result.getInstance()); } catch (Exception e) { e.printStackTrace(); }
		 * 
		 * try {
		 * 
		 * takeScreenshot(testMethodName, driver); } catch (IOException e) {
		 * 
		 * e.printStackTrace(); }
		 */
		 
		
		extentTestThread.get().fail(result.getThrowable());

		Object testClass = result.getInstance();
		if (testClass instanceof YourStoreBase) {
			WebDriver driver = ((YourStoreBase) testClass).driver;
			if (driver != null) {
				try {
					String screenshotFilePath = ((YourStoreBase) testClass).takeScreenshot(result.getMethod().getMethodName(), driver);
					extentTestThread.get().addScreenCaptureFromPath(screenshotFilePath, result.getMethod().getMethodName());
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("Driver is null, cannot take screenshot.");
			}
		} else {
			System.out.println("Test class does not extend YourStoreBase, cannot access driver.");
		}
		 
	}

	private YourStoreBase takeScreenshot(YourStoreBase testClass) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onTestSkipped(ITestResult result) {

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

	}

	@Override
	public void onTestFailedWithTimeout(ITestResult result) {

	}

	@Override
	public void onStart(ITestContext context) {

	}

	@Override
	public void onFinish(ITestContext context) {
		
		extentReport.flush();

	}

}
