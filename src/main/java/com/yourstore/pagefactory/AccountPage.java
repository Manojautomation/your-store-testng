package com.yourstore.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.yourstore.resources.YourStoreBase;

public class AccountPage extends YourStoreBase{
	
	public AccountPage(WebDriver driver) {
		
		this.driver=driver;
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(xpath="//a[contains(text(),'Edit your account information')]")
	WebElement editAccountInformation;
	
	public WebElement editAccountInformation(){
		
		return editAccountInformation;
	}

}
