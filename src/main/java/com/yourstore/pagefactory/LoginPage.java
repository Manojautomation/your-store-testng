package com.yourstore.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.yourstore.resources.YourStoreBase;

public class LoginPage extends YourStoreBase{
	
	public LoginPage(WebDriver driver) {
		
		this.driver=driver;
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(xpath="//input[@id='input-email']")
	private WebElement emailAddress;
	
	@FindBy(xpath="//input[@id='input-password']")
	private WebElement password;
	
	@FindBy(xpath="//input[@value='Login']")
	private WebElement loginBtn;
	
	public WebElement emailAddress(){
		
		return emailAddress;
	}
	
	public WebElement password(){
		
		return password;
	}

	public WebElement loginBtn(){
	
	return loginBtn;
	}

}
