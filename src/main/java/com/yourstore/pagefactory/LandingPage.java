package com.yourstore.pagefactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.yourstore.resources.YourStoreBase;

public class LandingPage extends YourStoreBase{
	
	public LandingPage(WebDriver driver) {
		
		this.driver=driver;
		PageFactory.initElements(driver, this);
		
	}
	
	@FindBy(xpath="//span[contains(text(),'My Account')]")
	WebElement myAccountDropdown;
	
	@FindBy(xpath="//a[contains(text(),'Login')]")
	WebElement loginOption;
	
	public WebElement myAccountDropdown(){
		
		return myAccountDropdown;
	}
	
	public WebElement loginOption(){
		
		return loginOption;
	}

}
