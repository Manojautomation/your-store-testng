package com.yourstore.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import org.apache.logging.log4j.Logger;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;


public class YourStoreBase {
	
	public	WebDriver driver;
	public Properties prop;
	

		
	public WebDriver intializeDriver() throws IOException {
		
		prop = new Properties();
		String propath= System.getProperty("user.dir")+"\\src\\main\\java\\com\\yourstore\\resources\\Data.properties";
		FileInputStream fis = new FileInputStream(propath);
		prop.load(fis);
		
		
		String browserName = prop.getProperty("browser");
		
		if(browserName.equalsIgnoreCase("Chrome")) {
			driver = new ChromeDriver();
		}
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		
		
		return driver;
	}
	
	public String takeScreenshot(String testName,WebDriver driver) throws IOException {
		
		/*
		 * File SourceFile = ((TakesScreenshot)
		 * driver).getScreenshotAs(OutputType.FILE); String destinationFilePath1 =
		 * System.getProperty("user.dir")+"\\screenshots\\"+testName+".png";
		 * FileUtils.copyFile(SourceFile,new File(destinationFilePath1));
		 * 
		 * 
		 * Screenshot screenshot1 = new
		 * AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).
		 * takeScreenshot(driver); ImageIO.write(screenshot1.getImage(), "PNG", new
		 * File(System.getProperty("user.dir")+"\\screenshots\\"+testName+".png"));
		 * FileUtils.copyFile(SourceFile,new File(destinationFilePath));
		 */
		 
	
		
		
		 
		 Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000)).takeScreenshot(driver);

	            // Temporary file to save the screenshot
	     File tempFile = File.createTempFile("screenshot", ".png");
	            
	            // Write the image to the temporary file
	     ImageIO.write(screenshot.getImage(), "PNG", tempFile);

	            // Destination path
	            String destinationFilePath = System.getProperty("user.dir")+"\\screenshots\\"+testName+".png";
	            File destinationFile = new File(destinationFilePath);

	            // Ensure the directory exists
	            File screenshotDir = destinationFile.getParentFile();
	            if (!screenshotDir.exists() && !screenshotDir.mkdirs()) {
	                throw new IOException("Failed to create directory: " + screenshotDir.getAbsolutePath());
	            }

	            // Copy the file to the final destination
	            FileUtils.copyFile(tempFile, destinationFile);
	            System.out.println("Screenshot saved: " + destinationFilePath);

	            return destinationFilePath;
	
	}
	
	

}
